var menu_btn = document.querySelector("#menu-btn");
var sidebar = document.querySelector("#sidebar");
var container = document.querySelector(".my-container");
menu_btn.addEventListener("click", () => {
  sidebar.classList.toggle("active-nav");
  container.classList.toggle("active-cont");
});

const token = "1/1201093248382751:e16a9ad30d7427374d21ed860aeef929";

const workspace_gid = "1201093208262895";

const baseUrl = "https://app.asana.com/api/1.0";

const allTasks = document.querySelector("#availableTasks");

const allTaskslist = document.querySelector(".allTaskslist");

allTasks.addEventListener("click", (e) => {
  allTaskslist.innerText = "";
  const MultipleProjectsurl = `${baseUrl}/tasks?assignee=guru.reddy.17@mountblue.tech&workspace=1201093208262895&opt_pretty=true&opt_fields=followers,assignee`;
  getMultipleTasks(MultipleProjectsurl, allTaskslist);
});

function getMultipleTasks(url, displayPlace) {
  taskGidArray = [];
  fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      data.data.forEach((element) => taskGidArray.push(Number(element.gid)));
      if (taskGidArray.length == 0) {
        const allTasksSpinner = document.querySelector("#allTasksSpinner");
        allTasksSpinner.style.display = "none";
        displayPlace.innerText = "No tasks available";
      } else {
        const taskUrl = `${baseUrl}/tasks`;
        getDueTasks(taskGidArray, displayPlace, taskUrl);
      }
    })
    .catch((err) => console.log(err));
}

function getDueTasks(taskGidArray, displayPlace, url) {
  taskGidArray.forEach((ele) => {
    fetch(`${url}/${ele}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const fragment = document.createDocumentFragment();
        li = document.createElement("li");
        checkBox = document.createElement("input");
        checkBox.type = "checkbox";
        checkBox.className = "mx-2 taskCheckBox";
        checkBox.id = ele
        a = document.createElement("a");
        a.append(data.data.name);
        a.classList.add(ele)
        // a.href = "#";
        a.setAttribute("data-bs-toggle", "offcanvas")
        a.setAttribute("data-bs-target", "#offcanvasRight")
        a.setAttribute("aria-controls", "offcanvasRight")

        a.style.color = "blue"
        a.style.textDecoration = "none";

        li.append(checkBox);
        li.className = "list-group-item"
        li.appendChild(a);

        //mark as complete event

        checkBox.addEventListener("click", e => {
          if (e.target.checked) {
            const target = document.getElementById(ele)
            target.parentElement.classList.remove("unchecked")
            target.parentElement.classList.add("checked")
          } else {
            const target = document.getElementById(ele)
            target.parentElement.classList.remove("checked")
            target.parentElement.classList.add("unchecked")
          }
        })
        //task details event
        a.addEventListener("click", event => {
          getTaskDetails(`${baseUrl}/tasks`, ele)
        })

        fragment.appendChild(li);
        const allTasksSpinner = document.querySelector("#allTasksSpinner");
        allTasksSpinner.style.display = "none";
        displayPlace.appendChild(fragment);
      })
      .catch((err) => console.log(err));
  });
}



function getTaskDetails(url, ele) {
  const fragment = document.createDocumentFragment()
  const taskDetailsBody = document.querySelector("#taskDetailsBody")
  taskDetailsBody.innerText = ""
  fetch(`${url}/${ele}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then(res => res.json())
    .then(data => {
      // console.log(data)
      const TaskSpinner = document.querySelector("#TaskSpinner");
      TaskSpinner.style.display = "none";
      const containerDiv = document.createElement("div")
      const taskNameDiv = document.createElement("div")
      const taskinfoDiv = document.createElement("div")
      const descriptionDiv = document.createElement("div")
      const subtaskDiv = document.createElement("div")
      const hr1 = document.createElement("hr")
      const hr2 = document.createElement("hr")
      const h1 = document.createElement("h1")
      const h6 = document.createElement("h6")
      const subtaskul = document.createElement("ul")
      const br = document.createElement("br")

      //taskName append
      h1.append(`${data.data.name}`)
      taskNameDiv.appendChild(h1)
      taskNameDiv.style.marginTop = "2rem"
      taskNameDiv.style.color = "chartreuse"
      containerDiv.appendChild(taskNameDiv)
      hr1.style.color = "blue"
      containerDiv.appendChild(hr1)

      //taskinfo append
      const infoArray = ['gid', 'created_at', 'completed', 'completed_at', 'due_on', 'due-at', 'resource_type', 'num_hearts', 'num_likes']

      for (let index in infoArray) {
        const li = document.createElement('li')
        li.style.color = "maroon"
        li.classList.add("py-2")
        li.innerText = `${infoArray[index]}: ${data.data[infoArray[index]]}`
        subtaskul.appendChild(li)
      }
      hr2.style.color = "green"
      subtaskul.append(hr2)
      taskinfoDiv.appendChild(subtaskul)


      //description append
      const descriptionheading = document.createElement("h1")
      descriptionheading.append("Description:")
      descriptionheading.style.color = "violet"
      descriptionDiv.append(descriptionheading)

      const descritionTextBox = document.createElement("textArea")
      descritionTextBox.style.border = "0"
      descritionTextBox.style.width = "100%"
      descritionTextBox.classList.add("py-2")
      descritionTextBox.classList.add("px-2")
      descritionTextBox.classList.add("text-primary")
      descritionTextBox.classList.add("lead")
      const hr3 = document.createElement("hr")
      hr3.style.color = "purple"
      if (data.data.notes !== "") {
        descritionTextBox.append(`${data.data.notes}`)
      }
      else {
        descritionTextBox.append("No added description to the task")
      }
      descritionTextBox.setAttribute("placeholder", `${data.data.notes}`)
      descriptionDiv.appendChild(descritionTextBox)
      descriptionDiv.appendChild(hr3)



      //subtasks append

      let subtaskUrl = `${baseUrl}/tasks/${data.data.gid}/subtasks`

      fetch(subtaskUrl, {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      })
        .then(res => res.json())
        .then(subdata => {
          const subtaskHeading = document.createElement("h1")
          subtaskHeading.append("Subtasks")
          subtaskHeading.style.color = "red"
          subtaskDiv.appendChild(subtaskHeading)
          subtaskHeading.classList.add("py-2")
          let subtaskul = document.createElement("ul")
          if (subdata.data.length !== 0) {
            subdata.data.forEach(ele => {
              li = document.createElement("li")
              li.classList.add("text-success")
              li.classList.add("py-2")
              li.classList.add("lead")
              li.append(ele.name)
              subtaskul.appendChild(li)
            })
          }
          else {
            li = document.createElement("li")
            li.classList.add("text-success")
            li.classList.add("py-2")
            li.classList.add("lead")
            li.append("subtasks are not available")
            subtaskul.appendChild(li)
          }
          subtaskDiv.appendChild(subtaskul)
        })

      //appending divs to fragment
      fragment.appendChild(containerDiv)
      fragment.appendChild(taskinfoDiv)
      fragment.appendChild(descriptionDiv)
      fragment.appendChild(subtaskDiv)

      //appending fragment to our target
      taskDetailsBody.appendChild(fragment)


      const subtaskButton = document.querySelector("#subtaskButton")

      subtaskButton.addEventListener("click", e => {
        const subtaskDescription = document.querySelector("#subtaskDescription").value
        const subtaskDueDate = document.querySelector("#subtaskDueDate").value
        const subtaskName = document.querySelector("#subtaskName").value
        const createSubtaskUrl = `${baseUrl}/tasks/${ele}/subtasks?opt_pretty=true&opt_fields=followers,assignee`
        createNewSubtask(subtaskUrl, subtaskName, subtaskDescription, subtaskDueDate)
      })
    })
}







function createNewSubtask(url, subtaskName, subtaskDescription, subtaskDueDate) {
  const bodyObject = {
    data: {
      approval_status: "pending",
      assignee: "guru.reddy.17@mountblue.tech",
      assignee_status: "upcoming",
      completed: false,
      due_on: subtaskDueDate,
      liked: true,
      name: subtaskName,
      notes: subtaskDescription,
      workspace: "1201093208262895",
    },
  };
  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(bodyObject),
  })
    .then(() => {
      const toastLiveExample = document.getElementById("liveToast");
      toastBody = document.querySelector(".toast-body");
      toastBody.innerText = "";
      h4 = document.createElement("h4");
      h4.innerText = "Subtask is successfully created";
      h4.className = "px-2";
      toastBody.appendChild(h4);
      var toast = new bootstrap.Toast(toastLiveExample);
      toast.show();
    })
    .catch((err) => alert(err.message));
}