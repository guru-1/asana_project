var menu_btn = document.querySelector("#menu-btn");
var sidebar = document.querySelector("#sidebar");
var container = document.querySelector(".my-container");
menu_btn.addEventListener("click", () => {
  sidebar.classList.toggle("active-nav");
  container.classList.toggle("active-cont");
});


const token = "1/1201093248382751:e16a9ad30d7427374d21ed860aeef929";

const workspace_gid = "1201093208262895";

const baseUrl = "https://app.asana.com/api/1.0";

const recentProjects = document.querySelector("#headingTwo");

const recenttaskslist = document.querySelector(".recentTaskslist");

recentProjects.addEventListener("click", () => {
  recenttaskslist.innerText = "";
  getProjectNames(recenttaskslist);
});

function getProjectNames(displayPlace) {
  const fragment = document.createDocumentFragment();
  fetch("https://app.asana.com/api/1.0/projects", {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      Object.entries(data).forEach((ele) => {
        ele[1].forEach((obj) => {
          li = document.createElement("li");
          a = document.createElement("a");
          a.append(obj["name"]);
          a.href = "./mytask.html";
          a.style.textDecoration = "none";
          li.className = "list-group-item"
          li.appendChild(a);
          fragment.appendChild(li);
        });
      });
      const projectSpinner = document.querySelector("#recentProjectsSpinner");
      projectSpinner.style.display = "none";
      displayPlace.appendChild(fragment);
    })
    .catch((err) => console.log(err));
}

const newTaskButton = document.querySelector("#taskButton");

newTaskButton.addEventListener("click", (e) => {
  const taskName = document.querySelector("#taskName").value;
  const taskDescription = document.querySelector("#taskDescription").value;
  const taskDueDate = document.querySelector("#taskDueDate").value;
  const url = `${baseUrl}/tasks?opt_pretty=true&opt_fields=followers,assignee`;
  if (taskName == "") {
    const toastLiveExample = document.getElementById("liveToast");
    toastBody = document.querySelector(".toast-body");
    toastBody.innerText = "";
    h4 = document.createElement("h4");
    h4.innerText = "Enter all required fields(*)";
    h4.className = "px-2";
    toastBody.appendChild(h4);
    var toast = new bootstrap.Toast(toastLiveExample);
    toast.show();
  } else {
    createNewSubtask(url, taskName, taskDescription, taskDueDate);
  }
});

function createNewSubtask(url, taskName, taskDescription, taskDueDate) {
  const bodyObject = {
    data: {
      approval_status: "pending",
      assignee: "guru.reddy.17@mountblue.tech",
      assignee_status: "upcoming",
      completed: false,
      due_on: taskDueDate,
      liked: true,
      name: taskName,
      notes: taskDescription,
      workspace: "1201093208262895",
    },
  };
  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify(bodyObject),
  })
    .then(() => {
      const toastLiveExample = document.getElementById("liveToast");
      toastBody = document.querySelector(".toast-body");
      toastBody.innerText = "";
      h4 = document.createElement("h4");
      h4.innerText = "Task is successfully created";
      h4.className = "px-2";
      toastBody.appendChild(h4);
      var toast = new bootstrap.Toast(toastLiveExample);
      toast.show();
    })
    .catch((err) => alert(err.message));
}

const tasksDue = document.querySelector("#headingOne");

const tasksDuelist = document.querySelector(".dueTaskslist");

tasksDue.addEventListener("click", (e) => {
  tasksDuelist.innerText = "";
  const MultipleProjectsurl = `${baseUrl}/tasks?assignee=guru.reddy.17@mountblue.tech&workspace=1201093208262895&opt_pretty=true&opt_fields=followers,assignee`;
  getMultipleTasks(MultipleProjectsurl, tasksDuelist);
});

function getMultipleTasks(url, displayPlace) {
  taskGidArray = [];
  fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      data.data.forEach((element) => taskGidArray.push(Number(element.gid)));
      if (taskGidArray.length == 0) {
        const tasksSpinner = document.querySelector("#dueTasksSpinner");
        tasksSpinner.style.display = "none";
        displayPlace.innerText = "No tasks due in next 5 days";
      } else {
        const taskUrl = `${baseUrl}/tasks`;
        taskGidArray.forEach(ele => {
          getDueTasks(ele, displayPlace, taskUrl);
        })
      }
    })
    .catch((err) => console.log(err));
}

function getDueTasks(ele, displayPlace, url) {
  fetch(`${url}/${ele}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      const fragment = document.createDocumentFragment();
      const dueDate = Date.parse(data.data.due_on);
      const today = Date.parse(new Date());
      if (dueDate - today < 5 * 86400000) {
        li = document.createElement("li");
        checkBox = document.createElement("input");
        checkBox.type = "checkbox";
        checkBox.className = "mx-2";
        checkBox.id = ele
        a = document.createElement("a");
        a.append(data.data.name);
        a.href = "./mytask.html";
        a.style.textDecoration = "none";
        li.className = "list-group-item unchecked"
        li.append(checkBox);
        li.appendChild(a);

        checkBox.addEventListener("click", e => {
          if (e.target.checked) {
            const target = document.getElementById(ele)
            target.parentElement.classList.remove("unchecked")
            target.parentElement.classList.add("checked")
          } else {
            const target = document.getElementById(ele)
            target.parentElement.classList.remove("checked")
            target.parentElement.classList.add("unchecked")
          }
        })
        fragment.appendChild(li);
      }
      const tasksSpinner = document.querySelector("#dueTasksSpinner");
      tasksSpinner.style.display = "none";

      displayPlace.appendChild(fragment);
    })
    .catch((err) => console.log(err));
}


